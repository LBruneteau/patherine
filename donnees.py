import pickle

class Donnees:

    def __init__(self) -> None:
        self.fideles = set()
        self.serveurs = set()

    def sauvegarder(self):
        with open('donnees.bin', 'wb') as fichier:
            pickle.dump(self, fichier)

    @classmethod
    def lire(cls):
        with open('donnees.bin', 'rb') as fichier:
            return pickle.load(fichier)