#!/home/pi/patherine/venv/bin/python3
'''Bot discord de Catherine de 12:06.
'''

import discord
import os
from discord.ext import commands, tasks
import dotenv
import datetime
import pytz
import logging
from donnees import Donnees
from fidele import Fidele
from serveur import Serveur

dotenv.load_dotenv()
TOKEN = os.getenv('TOKEN')
FUSEAU = pytz.timezone('Europe/Paris')
HEURE_CATHERINE = (12, 6)
STRING_CATHERINE = 'cath'
LWI = int(os.getenv('LWI'))
logging.basicConfig(filename='logs.txt', filemode='w',
                    format='%(asctime)s-%(levelname)s-%(message)s', level=logging.DEBUG)

bot = commands.Bot(command_prefix=';', intents=discord.Intents.all())
try:
    donnees = Donnees.lire()
    logging.info('Données lues')
except FileNotFoundError:
    donnees = Donnees()
    logging.info('Nouvelles données vierges')


@bot.command(name='role')
async def role(ctx, r: str):
    if not (ctx.author.guild_permissions.administrator or ctx.author.id == LWI):
        return
    serveur = ctx.guild
    if serveur.id not in [i.id for i in donnees.serveurs]:
        donnees.serveurs.add(Serveur(serveur.id))
        logging.info(f"Ajout du serveur {serveur.id}.")
        donnees.sauvegarder()
    serveur_associe = [i for i in donnees.serveurs if i.id == serveur.id][0]
    role_trouve = int(''.join([i for i in r if i.isnumeric()]))
    logging.info(f'role trouvé : {role_trouve}')
    serveur_associe.role = role_trouve
    donnees.sauvegarder()
    await ctx.message.add_reaction('💜')


async def on_message_catherine(message: discord.Message):
    if message.content.startswith(';'):
        return
    serveur = message.guild
    fidele = message.author
    heure = message.created_at.astimezone(FUSEAU)
    jour = datetime.date(heure.year, heure.month, heure.day)

    if serveur.id not in [i.id for i in donnees.serveurs]:
        donnees.serveurs.add(Serveur(serveur.id))
        logging.info(f"Ajout du serveur {serveur.id}.")
        donnees.sauvegarder()

    if not (heure.hour == HEURE_CATHERINE[0] and heure.minute == HEURE_CATHERINE[1]):
        return
    if not STRING_CATHERINE in message.content.lower() and '💜' not in message.content:
        return

    if fidele.id not in [i.id for i in donnees.fideles]:
        donnees.fideles.add(Fidele(fidele.id))
        logging.info(f"Ajout du fidèle {fidele.id}.")

    fidele_associe = [i for i in donnees.fideles if i.id == fidele.id][0]
    if not fidele_associe.cathou or fidele_associe.cathou[-1] != jour:
        fidele_associe.cathou.append(jour)
        donnees.sauvegarder()
    await message.add_reaction('💜')
    for serv in bot.guilds:
        try:
            serv_assoc = [i for i in donnees.serveurs if i.id == serv.id][0]
            if serv_assoc.role == None:
                continue
            for m in serv.members:
                if m.id == fidele.id:
                    await m.add_roles(discord.utils.get(serv.roles, id=serv_assoc.role))
                    serv_assoc.avec_role.add(m.id)
        except Exception:
            continue
    donnees.sauvegarder()
    logging.info(
        f'Catherine célébrée par {fidele.name} sur {serveur.name} le {jour}.')


bot.add_listener(on_message_catherine, 'on_message')


@tasks.loop(seconds=25)
async def midi_sept():
    try:
        heure = datetime.datetime.now().astimezone(FUSEAU)
        jour = datetime.date(heure.year, heure.month, heure.day)
        if heure.hour != 12 or heure.minute != 7:
            return
        logging.info("Midi sept")
        for serveur in donnees.serveurs:
            a_retirer = set()
            for membre in serveur.avec_role:
                for fidele in donnees.fideles:
                    if fidele.id == membre and (not fidele.cathou or jour != fidele.derniere_cathou()):
                        serv = discord.utils.get(bot.guilds, id=serveur.id)
                        m = discord.utils.get(serv.members, id=membre)
                        role = discord.utils.get(serv.roles, id=serveur.role)
                        a_retirer.add(membre)
                        await m.remove_roles(role)
                        logging.info(f'{m.name} perd le rôle sur {serv.name}')
            for membre in a_retirer:
                serveur.avec_role.remove(membre)
        donnees.sauvegarder()
    except Exception as _:
        logging.error('Erreur de midi sept', exc_info=True)


@bot.event
async def on_ready():
    midi_sept.start()

if __name__ == '__main__':
    bot.run(TOKEN)
